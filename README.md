#tomcat-redis-session
用于tomcat session持久化，把session存储到redis中，支持tomcat 7、tomcat 8.0和tomcat8.5

原项目不支持redis cluster,不支持tomcat8.0和tomcat8.5,详见 https://github.com/jcoleman/tomcat-redis-session-manager

#bin目录下为编译好的jar包(jdk1.8)，使用时放到tomcat的lib目录下即可	
#配置文件(conf/context.xml)样例


	<!--非集群模式配置-->
	<Valve className="cn.clinfo.tomcat.redissession.RedisSessionHandlerValve" />
	<Manager className="cn.clinfo.tomcat.redissession.RedisSessionManager"
		host="localhost"
		port="6379"
		database="0"
		maxInactiveInterval="60"
		sessionPersistPolicies="SAVE_ON_CHANGE,ALWAYS_SAVE_AFTER_REQUEST"
		sentinelMaster="SentinelMasterName"
		sentinels="sentinel-host-1:port,sentinel-host-2:port"/>


	<!-- 集群模式配置
	  serializationStrategyClass="cn.clinfo.tomcat.redissession.JavaSerializer" 默认序列化类
		clusterNodes="192.168.1.47:6380,192.168.1.47:6381" 集群节点
		password="password" redis密码
		maxRedirections="6" redis取值时最大Redirection次数
		connectionTimeout="2000" redis连接超时时长 毫秒
		soTimeout="2000" redis返回值超时时长  毫秒
		maxAttempts="5" redis出现异常最大重试次数
		maxWaitMillis="-1" redisPool 获取连接时的最大等待毫秒数(如果设置为阻塞时BlockWhenExhausted),如果超时就抛异常, 小于零:阻塞不确定的时间,  默认-1
		maxTotal="8" redisPool 最大连接数, 默认8个
		minIdle="8" redisPool 最小空闲连接数, 默认0
		maxIdle="100" redisPool 最大空闲连接数, 默认8个
		softMinEvictableIdleTimeMillis="1800000" redisPool对象空闲多久后逐出, 当空闲时间>该值 且 空闲连接>最大空闲数 时直接逐出,不再根据MinEvictableIdleTimeMillis判断  (默认逐出策略)
		testOnBorrow="false" redisPool 在获取连接的时候检查有效性, 默认false
		testWhileIdle="false" redisPool 在空闲时检查有效性, 默认false
		timeBetweenEvictionRunsMillis="-1" redisPool 逐出扫描的时间间隔(毫秒) 如果为负数,则不运行逐出线程, 默认-1
		maxInactiveInterval="180" session超时时长，单位秒默认180
		sessionPrefix="tomcat:session:" 记录到redis时sessionid的前缀
	-->
	
	<Valve className="cn.clinfo.tomcat.redissession.RedisClusterSessionHandlerValve"></Valve>
	<Manager className="cn.clinfo.tomcat.redissession.RedisClusterSessionManager"
	serializationStrategyClass="cn.clinfo.tomcat.redissession.JavaSerializer"
		clusterNodes="192.168.1.47:6380,192.168.1.47:6381"
		password="password"
		maxRedirections="6"
		connectionTimeout="7000"
		soTimeout="7000"
		maxAttempts="5"
		maxWaitMillis="-1"
		connectionPoolMaxTotal="500"
		connectionPoolMinIdle="8"
		connectionPoolMaxIdle="50"
		softMinEvictableIdleTimeMillis="1800000"
		testOnBorrow="false"
		testWhileIdle="false"
		timeBetweenEvictionRunsMillis="-1"
		maxInactiveInterval="1800"
		sessionPrefix="tomcat:session:"
		></Manager>
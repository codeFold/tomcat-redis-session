package cn.clinfo.tomcat.redissession;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.valves.ValveBase;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

public class RedisClusterSessionHandlerValve extends ValveBase {
	private final Log log = LogFactory.getLog(RedisClusterSessionManager.class);
	private RedisClusterSessionManager manager;

	public void setRedisSessionManager(RedisClusterSessionManager manager) {
		this.manager = manager;
	}

	@Override
	public void invoke(Request request, Response response) throws IOException, ServletException {
		try {
			getNext().invoke(request, response);
		} finally {
			manager.afterRequest();
		}
	}
}
